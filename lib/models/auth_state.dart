import 'package:meta/meta.dart';

import 'package:flutter_redux_boilerplate/models/user.dart';

@immutable
class AuthState {

    // properties
    final bool isAuthenticated;
    final bool isLoading;
    final User user;
    final String error;

    // constructor with default
    AuthState({
        this.isAuthenticated = false,
        this.isLoading = false,
        this.user,
        this.error,
    });

    // allows to modify AuthState parameters while cloning previous ones
    AuthState copyWith({
        bool isAuthenticated,
        bool isLoading,
        String error,
        User user
    }) {
        return new AuthState(
            isAuthenticated: isAuthenticated ?? this.isAuthenticated,
            isLoading: isLoading ?? this.isLoading,
            error: error ?? this.error,
            user: user ?? this.user,
        );
    }

    factory AuthState.fromJSON(Map<String, dynamic> json) => new AuthState(
        isAuthenticated: json['isAuthenticated'],
        isLoading: json['isLoading'],
        error: json['error'],
        user: json['user'] == null ? null : new User.fromJSON(json['user']),
    );

    Map<String, dynamic> toJSON() => <String, dynamic>{
        'isAuthenticated': this.isAuthenticated,
        'isLoading': this.isLoading,
        'user': this.user == null ? null : this.user.toJSON(),
        'error': this.error,
    };

    @override
    String toString() {
        return '''{
                isAuthenticated: $isAuthenticated,
                isLoading: $isLoading,
                user: $user,
                error: $error
            }''';
    }
}
