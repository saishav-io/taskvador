import 'package:flutter/material.dart';

import 'package:flutter_redux_boilerplate/presentation/platform_adaptive.dart';
import 'package:flutter_redux_boilerplate/styles/texts.dart';
import 'package:flutter_redux_boilerplate/screens/main_tabs/home_tab.dart';
import 'package:flutter_redux_boilerplate/screens/main_tabs/contacts_tab.dart';
import 'package:flutter_redux_boilerplate/screens/main_tabs/notifications_tab.dart';
import 'package:flutter_redux_boilerplate/screens/main_tabs/profile_tab.dart';
import 'package:flutter_redux_boilerplate/screens/main_drawer.dart';
import 'package:flutter_redux_boilerplate/containers/add_contact.dart';


class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  State<MainScreen> createState() => new MainScreenState();

}
class MainScreenState extends State<MainScreen> {
    
    PageController _tabController;
    String _title;
    int _index;
    Widget action;

    @override
    void initState() {
        super.initState();
        _tabController = new PageController();
        _title = TabItems[0].title;
        _index = 0;
    }

    @override
    Widget build(BuildContext context) {
        return new Scaffold(
            
            appBar: new PlatformAdaptiveAppBar(
                title: new Text(_title),
                platform: Theme.of(context).platform,
                actions: <Widget>[
                    IconButton(
                        icon: Icon(Icons.person_add),
                        onPressed: () {
                            displayDialog(context);
                        },
                    )
                ],
            ),
            
            bottomNavigationBar: new PlatformAdaptiveBottomBar(
                currentIndex: _index,
                onTap: onTap,
                items: TabItems.map((TabItem item) {
                    return new BottomNavigationBarItem(
                        title: new Text(
                            item.title,
                            style: textStyles['bottom_label'],
                        ),
                        icon: new Icon(item.icon),
                    );
                }).toList(),
            ),

            body: new PageView(
                controller: _tabController,
                onPageChanged: onTabChanged,
                children: <Widget>[
                    new HomeTab(),
                    new NetworkTab(),
                    new NotificationsTab(),
                    new ProfileTab()
                ],
            ),

            drawer: new MainDrawer(),
        );
    }

    void onTap(int tab){
        _tabController.jumpToPage(tab);
    }

    void onTabChanged(int tab) {
        setState((){
            this._index = tab;
        });
        
        this._title = TabItems[tab].title;
    }

}

class TabItem {
    final String title;
    final IconData icon;

    const TabItem({ this.title, this.icon });
}

const List<TabItem> TabItems = const <TabItem>[
    const TabItem(title: 'Home', icon: Icons.home),
    const TabItem(title: 'Contacts', icon: Icons.contacts),
    const TabItem(title: 'Notifications', icon: Icons.notifications),
    const TabItem(title: 'Me', icon: Icons.account_circle)
];
