import 'package:flutter/material.dart';

class NetworkTab extends StatelessWidget {
    NetworkTab({Key key}) : super(key: key);

    final List<String> contacts = ['amanda', 'nader', 'jennifer'];

    @override
    Widget build(BuildContext context) {
        return new Center(
            child: ListView.builder(
                itemCount: this.contacts.length,
                itemBuilder: (BuildContext context, index) {
                    return ListTile(
                        leading: const Icon(Icons.account_circle),
                        title: Text(contacts[index]),
                        subtitle: const Text('User description here'),
                        onTap: () { /* react to the tile being tapped */ }
                    );
                }
            ),
        );
    }

}
