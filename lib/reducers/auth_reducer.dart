import 'package:redux/redux.dart';

import 'package:flutter_redux_boilerplate/actions/auth_actions.dart';
import 'package:flutter_redux_boilerplate/models/auth_state.dart';

Reducer<AuthState> authReducer = combineReducers([
    new TypedReducer<AuthState, UserSignUpRequest>(userSignUpRequestReducer),
    new TypedReducer<AuthState, UserSignUpSuccess>(userSignUpSuccessReducer),
    new TypedReducer<AuthState, UserSignUpFailure>(userSignUpFailureReducer),
    new TypedReducer<AuthState, UserLoginRequest>(userLoginRequestReducer),
    new TypedReducer<AuthState, UserLoginSuccess>(userLoginSuccessReducer),
    new TypedReducer<AuthState, UserLoginFailure>(userLoginFailureReducer),

    new TypedReducer<AuthState, UserLogout>(userLogoutReducer),
]);

AuthState userSignUpRequestReducer(AuthState auth, UserSignUpRequest action) {
    return new AuthState().copyWith(
        isLoading: true,
    );
}

AuthState userSignUpSuccessReducer(AuthState auth, UserSignUpSuccess action) {
    return new AuthState().copyWith(
        isLoading: false,
        user: action.user
    );
}

AuthState userSignUpFailureReducer(AuthState auth, UserSignUpFailure action) {
    return new AuthState().copyWith(
        isLoading: false,
        error: action.error
    );
}


AuthState userLoginRequestReducer(AuthState auth, UserLoginRequest action) {
    return new AuthState().copyWith(
        isAuthenticated: false,
        isLoading: true,
    );
}

AuthState userLoginSuccessReducer(AuthState auth, UserLoginSuccess action) {
    return new AuthState().copyWith(
        isAuthenticated: true,
        isLoading: false,
        user: action.user
    );
}

AuthState userLoginFailureReducer(AuthState auth, UserLoginFailure action) {
    return new AuthState().copyWith(
        isAuthenticated: false,
        isLoading: false,
        error: action.error
    );
}

AuthState userLogoutReducer(AuthState auth, UserLogout action) {
    return new AuthState();
}
