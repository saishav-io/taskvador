import 'package:flutter/material.dart';

Map<String, Color> colorStyles = {
    'primary_dark': Colors.orange[700],
    'primary': Colors.orange,
    'ligth_font': Colors.black54,
    'gray': Colors.black45,
    'white': Colors.white
};
