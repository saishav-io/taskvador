import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

import 'package:flutter_redux_boilerplate/models/user.dart';
import 'package:flutter_redux_boilerplate/models/app_state.dart';

class UserLoginRequest {}

class UserSignUpRequest {}

class UserLoginSuccess {
    final User user;

    UserLoginSuccess(this.user);
}

class UserSignUpSuccess {
    final User user;

    UserSignUpSuccess(this.user);
}

class UserLoginFailure {
    final String error;

    UserLoginFailure(this.error);
}

class UserSignUpFailure {
    final String error;

    UserSignUpFailure(this.error);
}

class UserLogout {}

final Function signUp = (BuildContext context, String username, String password) {
    return (Store<AppState> store) {
        store.dispatch(new UserSignUpRequest());
        store.dispatch(register(context, username, password));
    };
};

final Function login = (BuildContext context, String username, String password) {
    return (Store<AppState> store) {
        store.dispatch(new UserLoginRequest());
        store.dispatch(authenticate(context, username, password));
    };
};

final Function logout = (BuildContext context) {
    return (Store<AppState> store) {
        store.dispatch(signout(context));
    };
};

ThunkAction<AppState> register(BuildContext context, String username, String password) {
    return (Store<AppState> store) async {
        try {
            FirebaseUser user = await FirebaseAuth.instance
                .createUserWithEmailAndPassword(
                email: username, password: password);

            FirebaseDatabase.instance.reference()
                .child('users/')
                .push()
                .set({
                'email': user.email,
            });

            store.dispatch(new UserSignUpSuccess(new User('placeholder_token', user.email)));
            Navigator.of(context).pushNamedAndRemoveUntil('/login', (_) => false);
        } catch (e) {
            Scaffold.of(context).showSnackBar(SnackBar(content: Text('Unable to register user')));
            store.dispatch(new UserSignUpFailure('Unable to register user'));
        }

    };
}

ThunkAction<AppState> authenticate(BuildContext context, String username, String password) {
    return (Store<AppState> store) async {
        try {
            await FirebaseAuth.instance
                .signInWithEmailAndPassword(
                email: username, password: password);
            store.dispatch(new UserLoginSuccess(new User('placeholder_token', username)));
            Navigator.of(context).pushNamedAndRemoveUntil('/main', (_) => false);
        } catch (e) {
            Scaffold.of(context).showSnackBar(SnackBar(content: Text('Username or password were incorrect.')));
            store.dispatch(new UserLoginFailure('Username or password were incorrect.'));
        }

    };
}

ThunkAction<AppState> signout(BuildContext context) {
    return (Store<AppState> store) async {
        try {
            await FirebaseAuth.instance.signOut();
            store.dispatch(new UserLogout());
            Navigator.of(context).pushNamedAndRemoveUntil('/login', (_) => false);
        } catch (e) {
            Scaffold.of(context).showSnackBar(SnackBar(content: Text('Error while signing out, please try again')));

        }

    };
}



